import time
import requests
from RandomDealData import RandomDealData
# from dataGen.RandomDealData import RandomDealData



dao_url_str = '10.1.15.164'
dao_port_str = '8080'
add_str = 'addDeal'

delay = 3  # in s
amount = 20000


def bootapp1():
    # app.run(port=9001, threaded=True, host=('0.0.0.0'))
    generator = RandomDealData()
    instruments = generator.createInstrumentList()
    for i in range(amount):
        data = generator.createRandomData(instruments)
        requests.get('http://' + dao_url_str + ':' + dao_port_str + '/'
                     + add_str + '/' + str(data), data)
        time.sleep(delay)

def bootapp():
    generator = RandomDealData()
    instruments = generator.createInstrumentList()
    for i in range(amount):
        json_data = generator.createRandomData(instruments)
        response = requests.post('http://' + dao_url_str + ':' + dao_port_str + '/' +
                      add_str, json=json_data)
        print(json_data)
        print(response)
        time.sleep(delay)


if __name__ == '__main__':
    bootapp()



