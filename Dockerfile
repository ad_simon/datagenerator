FROM python:3
 
FROM ubuntu:latest
LABEL Thomson Kneeland "thomsonkneeland@yahoo.com"
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
CMD [ "python", "./db_ws_stream.py" ]