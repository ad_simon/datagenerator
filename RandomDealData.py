import time
import numpy, random
from datetime import datetime, timedelta
import json
from Instrument import *
# from dataGen.Instrument import *

instruments = ("Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
			"Floral", "Galactica", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic")
counterparties = ("Lewis", "Selvyn", "Richard", "Lina", "John", "Nidia")
NUMBER_OF_RANDOM_DEALS = 2000
TIME_PERIOD_MILLIS = 3600000
EPOCH = datetime.now() - timedelta(days = 1)

class   RandomDealData:
    dealId = 2000
    def createInstrumentList(self):
        f = open('initialRandomValues.txt', 'r')
        # f = open('dataGen/initialRandomValues.txt', 'r')

        instrumentId = 1000
        instrumentList = []
        for instrumentName in instruments:
            hashedValue = int(f.readline())
            isNegative = hashedValue < 0
            basePrice = (abs(hashedValue) % 10000) + 90.0
            drift = ((abs(hashedValue) % 5) * basePrice) / 1000.0
            drift = 0 - drift if isNegative else drift
            variance = (abs(hashedValue) % 1000) / 100.0
            variance = 0 - variance if isNegative else variance
            instrument = Instrument(instrumentName, basePrice, drift, variance)
            instrumentList.append(instrument)
            instrumentId += 1
        return instrumentList
      
    def createRandomData( self, instrumentList ):
        time.sleep(random.uniform(1,30)/100)
        instrument = instrumentList[numpy.random.randint(0,len(instrumentList))]
        cpty = counterparties[numpy.random.randint(0,len(counterparties))]
        type = 'B' if numpy.random.choice([True, False]) else 'S'
        quantity = int( numpy.power(1001, numpy.random.random()))
        dealTime = datetime.now() - timedelta(days = 1)
        self.dealId += 1
        deal = {
            'deal_id': self.dealId,
            'instrumentName' : instrument.getName(),
            'cpty' : cpty,
            'price' : instrument.calculateNextPrice(type),
            'type' : type,
            'quantity' : quantity,
            'time' : dealTime.strftime("%Y-%m-%d %H:%M:%S.%f"),
            }
        return json.dumps(deal)


    # "2019-08-08 16:21:41.253907"  datetime in DB

# Data Generation  (EXAMPLE, NOT FINAL)
"""
x = RandomDealData()
instrumentList = x.createInstrumentList()
for i in range(2000):
    print(x.createRandomData(instrumentList))
"""